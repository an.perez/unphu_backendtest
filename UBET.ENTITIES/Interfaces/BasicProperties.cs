﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBET.ENTITIES.Interfaces
{
    public class BasicProperties
    {

        public Guid GID { get; set; }

        [Key]
        public int Indx { get; set; }

        public bool Estatus { get; set; }

        public DateTime FechaRegistro { get; set; }

        public Guid RegistradoPor { get; set; }

    }
}
