using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UBET.ENTITIES.Interfaces;

namespace UBET.ENTITIES
{
    [Table("TipoGenerales")]
    public partial class TipoGenerales: BasicProperties
    {
        [StringLength(10)]
        public string Codigo { get; set; }

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        public Guid? PadreId { get; set; }

    }
}
