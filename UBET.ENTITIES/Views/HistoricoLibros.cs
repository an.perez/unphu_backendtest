﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBET.ENTITIES.Views
{
    public partial class HistoricoLibros
    {
        public Guid GID { get; set; }
        public string Libro { get; set; }
        public string Epilogo { get; set; }
        public string Autor { get; set; }
        public DateTime FechaPublicacion { get; set; }
        public int ClasificacionTotal { get; set; }
        public int VotosAFavor { get; set; }
        public int VotosEnContra { get; set; }
    }
}
