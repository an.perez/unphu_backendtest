using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UBET.ENTITIES.Interfaces;

namespace UBET.ENTITIES
{
    [Table("Usuarios")]
    public partial class Usuario : BasicProperties
    {
        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(75)]
        public string Apellido { get; set; }

        [Required]
        [StringLength(150)]
        public string Correo { get; set; }

        [Required]
        public byte[] Contrasena { get; set; }

        [Column(TypeName = "date")]
        public DateTime FechaNacimiento { get; set; }

        public bool Estudiante { get; set; }

        [StringLength(25)]
        public string Matricula { get; set; }

        public Guid? Carrera { get; set; }

    }
}
