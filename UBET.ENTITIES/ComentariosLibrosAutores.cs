using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UBET.ENTITIES.Interfaces;

namespace UBET.ENTITIES
{
    [Table("ComentariosLibrosAutores")]
    public partial class ComentariosLibrosAutores: BasicProperties
    {
        public int Rating { get; set; }

        public Guid OwnerId { get; set; }

        [Required]
        [StringLength(500)]
        public string Comentario { get; set; }

    }
}
