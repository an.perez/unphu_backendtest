using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UBET.ENTITIES.Interfaces;

namespace UBET.ENTITIES
{
    [Table("Libros")]
    public partial class Libro: BasicProperties
    {
        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }

        [Required]
        public string Epilogo { get; set; }

        public Guid Autor { get; set; }

        public int GeneroLiterario { get; set; }

        public DateTime FechaPublicacion { get; set; }

        public int ClasificacionTotal { get; set; }

        public int VotosAFavor { get; set; }

        public int VotosEnContra { get; set; }

    }
}
