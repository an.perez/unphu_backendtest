using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UBET.ENTITIES.Interfaces;

namespace UBET.ENTITIES
{
    [Table("Autores")]
    public partial class Autor : BasicProperties
    {
        [Required]
        [StringLength(65)]
        public string Nombre { get; set; }

        //[StringLength(250)]
        public string Descripcion { get; set; }

        public int? GeneroLiterario { get; set; }

    }
}
