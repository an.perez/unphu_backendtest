﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBET.ENTITIES;

namespace UBET.DATA
{
    public partial class AutoresMap: _AdapterBase<Autor>
    {
        #region Static Properties
        public static AutoresMap Current { get { return new AutoresMap(); } }
        #endregion

        public AutoresMap()
        {
            this.HasKey(t => t.Indx);


            this.ToTable(entityName);
            this.Property(t => t.GID);
            this.Property(t => t.Indx);
            this.Property(t => t.Nombre);
            this.Property(t => t.Descripcion);
            this.Property(t => t.GeneroLiterario);
            this.Property(t => t.Estatus);
            this.Property(t => t.FechaRegistro);
            this.Property(t => t.RegistradoPor);

        }
    }
}
