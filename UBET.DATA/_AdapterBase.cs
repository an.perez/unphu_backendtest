﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UBET.DATA.Interfaces;
using UBET.ENTITIES.Interfaces;
using UBET.UTILITY;

namespace UBET.DATA
{
    public partial class _AdapterBase<TEntity> : EntityTypeConfiguration<TEntity>, IAdapterBase<TEntity> where TEntity : BasicProperties, new()
    {
        #region [Public and Private Properties]

        private edmBiblioteca db;

        //public static TEntity Current { get { return new TEntity(); } }

        public string entityName = (new TEntity()).GetTableName();

        #endregion

        public virtual List<TEntity> GetAll()
        {
            List<TEntity> entities = new List<TEntity>();

            try
            {
                using (db = new edmBiblioteca())
                {
                    var tblObjects = from row in ((IObjectContextAdapter)db).ObjectContext.CreateQuery<TEntity>("[" + entityName + "]") select row;

                    if (tblObjects.Any())
                    {
                        foreach (var tbl in tblObjects)
                        {
                            TEntity te = ObjectUtility.FillProperties<TEntity>(tbl);

                            entities.Add(te);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return entities;
        }

        public virtual List<TEntity> Find(Expression<Func<TEntity, bool>> condition)
        {
            List<TEntity> entities = new List<TEntity>();
            try
            {
                using (db = new edmBiblioteca())
                {
                    var tblObjects = from row in ((IObjectContextAdapter)db).ObjectContext.CreateQuery<TEntity>("[" + entityName + "]").Where<TEntity>(condition) select row;

                    if (tblObjects.Any())
                    {
                        foreach (var tbl in tblObjects)
                        {
                            TEntity te = ObjectUtility.FillProperties<TEntity>(tbl);

                            entities.Add(te);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return entities;
        }

        public virtual TEntity Add(TEntity entity)
        {
            return this.Add(new List<TEntity>() { entity }).FirstOrDefault();
        }

        public virtual List<TEntity> Add(IEnumerable<TEntity> entities)
        {
            List<TEntity> result = new List<TEntity>();

            if (!entities.Any()) { return result; }

            using (db = new edmBiblioteca())
            {
                try
                {
                    foreach (var item in entities)
                    {
                        if (item.GID == Guid.Empty)
                        {
                            item.GID = Guid.NewGuid();
                        }

                        item.FechaRegistro = DateTime.Now;
                        item.Estatus = true;

                        TEntity tblObject = ObjectUtility.FillProperties<TEntity>(item as TEntity);

                        result.Add(db.Set<TEntity>().Add(tblObject));

                    }
                    if (db.SaveChanges() > 0)
                    {
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return result;
        }

        public virtual TEntity Update(TEntity entity)
        {
            return this.Update(new List<TEntity>() { entity }).FirstOrDefault();
        }

        public virtual List<TEntity> Update(IEnumerable<TEntity> entities)
        {
            List<TEntity> result = new List<TEntity>();

            if (!entities.Any()) { return result; }

            using (db = new edmBiblioteca())
            {
                try
                {
                    foreach (var item in entities)
                    {
                        TEntity tblObject = ObjectUtility.FillProperties<TEntity>(item as TEntity);


                        ((IObjectContextAdapter)db).ObjectContext.AttachTo(entityName, tblObject);
                        ((IObjectContextAdapter)db).ObjectContext.ObjectStateManager.ChangeObjectState(tblObject, System.Data.Entity.EntityState.Modified);

                    }

                    if (db.SaveChanges() > 0)
                    {
                        result.AddRange(entities);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                //catch (DbEntityValidationException e)
                //{
                //    foreach (var eve in e.EntityValidationErrors)
                //    {
                //        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                //            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                //        foreach (var ve in eve.ValidationErrors)
                //        {
                //            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                //                ve.PropertyName, ve.ErrorMessage);
                //        }
                //    }
                //    throw;
                //}
            }

            return result;
        }

        public virtual bool Delete(TEntity entity)
        {
            return this.Delete(new List<TEntity>() { entity });
        }

        public virtual bool Delete(IEnumerable<TEntity> entities)
        {
            bool result = false;

            if (!entities.Any()) { return result; }

            using (db = new edmBiblioteca())
            {
                try
                {
                    foreach (var item in entities)
                    {
                        TEntity tblObject = ObjectUtility.FillProperties<TEntity>(item as TEntity);


                        ((IObjectContextAdapter)db).ObjectContext.AttachTo(entityName, tblObject);
                        ((IObjectContextAdapter)db).ObjectContext.ObjectStateManager.ChangeObjectState(tblObject, System.Data.Entity.EntityState.Deleted);

                    }

                    if (db.SaveChanges() > 0)
                    {
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return result;
        }

        public virtual void ExecProcedureNotReturnData(string procedureName)
        {
            using (db = new edmBiblioteca())
            {
                try
                {
                    ((IObjectContextAdapter)db).ObjectContext.ExecuteStoreCommand(procedureName);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public virtual List<T> CallSelectStoredProcedure<T>(string procedureName) where T : class, new()
        {
            using (db = new edmBiblioteca())
            {
                try
                {
                    return ((IObjectContextAdapter)db).ObjectContext.ExecuteStoreQuery<T>(procedureName).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
