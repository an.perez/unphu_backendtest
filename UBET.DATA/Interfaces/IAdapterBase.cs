﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace UBET.DATA.Interfaces
{
    interface IAdapterBase<Tentity> where Tentity : class, new()
    {
        List<Tentity> GetAll();
        List<Tentity> Find(Expression<Func<Tentity, bool>> condition);
        Tentity Add(Tentity entity);
        List<Tentity> Add(IEnumerable<Tentity> entities);
        Tentity Update(Tentity entity);
        List<Tentity> Update(IEnumerable<Tentity> entities);
        bool Delete(Tentity entity);
        bool Delete(IEnumerable<Tentity> entities);

    }
}
