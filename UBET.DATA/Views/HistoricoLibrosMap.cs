﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBET.ENTITIES.Views;

namespace UBET.DATA
{
    public partial class HistoricoLibrosMap : EntityTypeConfiguration<HistoricoLibros>
    {
        public HistoricoLibrosMap()
        {
            this.HasKey(t => t.GID);

            this.ToTable("HistoricoLibros");
            this.Property(t => t.GID);
            this.Property(t => t.Libro);
            this.Property(t => t.Epilogo);
            this.Property(t => t.Autor);
            this.Property(t => t.FechaPublicacion);
            this.Property(t => t.ClasificacionTotal);
            this.Property(t => t.VotosAFavor);
            this.Property(t => t.VotosEnContra);
        }
    }
}
