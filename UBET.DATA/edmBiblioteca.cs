namespace UBET.DATA
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using UBET.ENTITIES;
    using UBET.ENTITIES.Views;

    public partial class edmBiblioteca : DbContext
    {
        public edmBiblioteca()
            : base("name=edmBiblioteca")
        {
        }

        //Tables
        public virtual DbSet<Autor> Autores { get; set; }
        public virtual DbSet<ComentariosLibrosAutores> ComentariosLibrosAutores { get; set; }
        public virtual DbSet<Libro> Libros { get; set; }
        public virtual DbSet<TipoGenerales> TipoGenerales { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }

        //Views
        public virtual DbSet<HistoricoLibros> HistoricoLibros { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Tables
            modelBuilder.Configurations.Add(new AutoresMap());
            modelBuilder.Configurations.Add(new ComentariosLibrosAutoresMap());
            modelBuilder.Configurations.Add(new LibrosMap());
            modelBuilder.Configurations.Add(new TipoGeneralesMap());
            modelBuilder.Configurations.Add(new UsuariosMap());

            //Views 
            modelBuilder.Configurations.Add(new HistoricoLibrosMap());
        }
    }
}
