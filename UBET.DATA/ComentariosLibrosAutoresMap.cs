﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBET.ENTITIES;

namespace UBET.DATA
{
    public partial class ComentariosLibrosAutoresMap:_AdapterBase<ComentariosLibrosAutores>
    {
        #region Static Properties
        public static ComentariosLibrosAutoresMap Current { get { return new ComentariosLibrosAutoresMap(); } }
        #endregion

        public ComentariosLibrosAutoresMap()
        {
            this.HasKey(t => t.Indx);


            this.ToTable(entityName);
            this.Property(t => t.GID);
            this.Property(t => t.Indx);
            this.Property(t => t.Rating);
            this.Property(t => t.OwnerId);
            this.Property(t => t.Comentario);
            this.Property(t => t.Estatus);
            this.Property(t => t.FechaRegistro);
            this.Property(t => t.RegistradoPor);

        }
    }
}
