﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBET.ENTITIES;

namespace UBET.DATA
{
    public partial class LibrosMap:_AdapterBase<Libro>
    {
        #region Static Properties
        public static LibrosMap Current { get { return new LibrosMap(); } }
        #endregion

        public LibrosMap()
        {
            this.HasKey(t => t.Indx);


            this.ToTable(entityName);
            this.Property(t => t.GID);
            this.Property(t => t.Indx);
            this.Property(t => t.Nombre);
            this.Property(t => t.Epilogo);
            this.Property(t => t.Autor);
            this.Property(t => t.GeneroLiterario);
            this.Property(t => t.FechaPublicacion);
            this.Property(t => t.ClasificacionTotal);
            this.Property(t => t.VotosAFavor);
            this.Property(t => t.VotosEnContra);
            this.Property(t => t.Estatus);
            this.Property(t => t.FechaRegistro);
            this.Property(t => t.RegistradoPor);

        }
    }
}
