﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBET.ENTITIES;

namespace UBET.DATA
{
    public partial class TipoGeneralesMap: _AdapterBase<TipoGenerales>
    {
        #region Static Properties
        public static TipoGeneralesMap Current { get { return new TipoGeneralesMap(); } }
        #endregion

        public TipoGeneralesMap()
        {
            this.HasKey(t => t.Indx);


            this.ToTable(entityName);
            this.Property(t => t.GID);
            this.Property(t => t.Indx);
            this.Property(t => t.Codigo);
            this.Property(t => t.Nombre);
            this.Property(t => t.PadreId);
            this.Property(t => t.Estatus);
            this.Property(t => t.FechaRegistro);
            this.Property(t => t.RegistradoPor);

        }
    }
}
