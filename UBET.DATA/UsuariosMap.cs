﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBET.ENTITIES;

namespace UBET.DATA
{
    public partial class UsuariosMap: _AdapterBase<Usuario>
    {
        #region Static Properties
        public static UsuariosMap Current { get { return new UsuariosMap(); } }
        #endregion

        public UsuariosMap()
        {
            this.HasKey(t => t.Indx);


            this.ToTable(entityName);
            this.Property(t => t.GID);
            this.Property(t => t.Indx);
            this.Property(t => t.Nombre);
            this.Property(t => t.Apellido);
            this.Property(t => t.Correo);
            this.Property(t => t.Contrasena);
            this.Property(t => t.FechaNacimiento);
            this.Property(t => t.Estudiante);
            this.Property(t => t.Matricula);
            this.Property(t => t.Carrera);
            this.Property(t => t.Estatus);
            this.Property(t => t.FechaRegistro);
            this.Property(t => t.RegistradoPor);

        }
    }
}
