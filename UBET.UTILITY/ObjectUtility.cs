﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UBET.UTILITY
{
    public static class ObjectUtility
    {

        public static T FillProperties<T>(T obj, bool changeId = false) where T : new()
        {
            var newTobj = new T();
            Type nObjType = newTobj.GetType();
            Type mObjType = obj.GetType();

            System.Reflection.PropertyInfo[] tObjPropertiesInfo = nObjType.GetProperties();
            System.Reflection.PropertyInfo[] mObjPropertiesInfo = mObjType.GetProperties();

            foreach (System.Reflection.PropertyInfo info in mObjPropertiesInfo)
            {
                var tp = tObjPropertiesInfo.FirstOrDefault(x => x.Name == info.Name);

                if (tp != null)
                {
                    if (tp.PropertyType.FullName == info.PropertyType.FullName)
                    {
                        tp.SetValue(newTobj, info.GetValue(obj, null), null);
                    }
                    else if (info.PropertyType.BaseType.Name.ToLower() == "enum")
                    {
                        tp.SetValue(newTobj, info.GetValue(obj, null).ToString(), null);
                    }
                    else if (tp.PropertyType.BaseType.Name.ToLower() == "enum")
                    {
                        tp.SetValue(newTobj, Enum.Parse(tp.PropertyType, info.GetValue(obj, null).ToString()));
                    }
                }
            }

            return newTobj;
        }

        public static string GetTableName<T>(this T obj) where T : class, new()
        {
            string tableName = typeof(T).Name;

            var customAttributes = typeof(T).GetTypeInfo().GetCustomAttributes<System.ComponentModel.DataAnnotations.Schema.TableAttribute>();
            if (customAttributes.Count() > 0)
            {
                tableName = customAttributes.First().Name;
            }

            return tableName;
        }
    }
}
