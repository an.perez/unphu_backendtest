﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using UBET.DATA;
using UBET.ENTITIES;
using UBET.SERVICES.Interfaces;

namespace UBET.SERVICES
{
    public class ComentariosLibrosAutoresServices : IBasicMethods<ComentariosLibrosAutores>
    {
        #region Static Properties
        public static ComentariosLibrosAutoresServices Current { get { return new ComentariosLibrosAutoresServices(); } }
        #endregion

        /// <summary>
        /// Method to get a list of Comments
        /// </summary>
        /// <returns>List of Comentarios</returns>
        public IEnumerable<ComentariosLibrosAutores> GetAll()
        {
            return ComentariosLibrosAutoresMap.Current.GetAll();
        }

        /// <summary>
        /// Method to get a comment by GID
        /// </summary>
        /// <param name="id">Guid: Comment GID</param>
        /// <returns>A comment</returns>
        public ComentariosLibrosAutores Find(Guid id)
        {
            return ComentariosLibrosAutoresMap.Current.Find(x => x.GID == id).FirstOrDefault();
        }

        /// <summary>
        /// Method to get a list comments by owner
        /// </summary>
        /// <param name="id">Guid: owner GID</param>
        /// <returns>List of comments</returns>
        public List<ComentariosLibrosAutores> FindByOwner(Guid ownerId)
        {
            return ComentariosLibrosAutoresMap.Current.Find(x => x.OwnerId == ownerId);
        }

        /// <summary>
        /// Method to add a new comment
        /// </summary>
        /// <param name="entity">Comment to be save</param>
        /// <returns>A comment saved</returns>
        public ComentariosLibrosAutores Add(ComentariosLibrosAutores entity)
        {
            return this.Add(new List<ComentariosLibrosAutores>() { entity }).FirstOrDefault();
        }

        /// <summary>
        /// Method to adds a list of comments
        /// </summary>
        /// <param name="entities">List of commets to be save</param>
        /// <returns>A list of comments saved</returns>
        public IEnumerable<ComentariosLibrosAutores> Add(IEnumerable<ComentariosLibrosAutores> entities)
        {
            List<ComentariosLibrosAutores> result = new List<ComentariosLibrosAutores>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    result = ComentariosLibrosAutoresMap.Current.Add(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return result;
        }

        /// <summary>
        /// Method update a comment
        /// </summary>
        /// <param name="entity">Comment to be updated</param>
        /// <returns>A comment updated</returns>
        public ComentariosLibrosAutores Update(ComentariosLibrosAutores entity)
        {
            return this.Update(new List<ComentariosLibrosAutores>() { entity }).FirstOrDefault();
        }

        /// <summary>
        /// Method to update a list of comments
        /// </summary>
        /// <param name="entities">Comments to be updated</param>
        /// <returns>A comments updated</returns>
        public IEnumerable<ComentariosLibrosAutores> Update(IEnumerable<ComentariosLibrosAutores> entities)
        {
            List<ComentariosLibrosAutores> result = new List<ComentariosLibrosAutores>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    result = ComentariosLibrosAutoresMap.Current.Update(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return result;
        }

        /// <summary>
        /// Methos to remove a commet
        /// </summary>
        /// <param name="id">Guid: Comment GID</param>
        /// <returns>True if the comment was removed</returns>
        public bool Delete(Guid id)
        {
            return this.Delete(new List<Guid>() { id });
        }

        /// <summary>
        /// Method to remove a list of comments
        /// </summary>
        /// <param name="ids">List of Commmets GID</param>
        /// <returns>True if the comments was removed</returns>
        public bool Delete(IEnumerable<Guid> ids)
        {
            bool result = false;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    //Get comment to be removed
                    var entities = ComentariosLibrosAutoresMap.Current.Find(x => ids.Contains(x.GID));

                    result = ComentariosLibrosAutoresMap.Current.Delete(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return result;
        }
    }
}
