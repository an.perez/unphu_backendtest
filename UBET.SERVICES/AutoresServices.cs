﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using UBET.DATA;
using UBET.ENTITIES;
using UBET.SERVICES.Interfaces;

namespace UBET.SERVICES
{
    public class AutoresServices : IBasicMethods<Autor>
    {
        #region Static Properties
        public static AutoresServices Current { get { return new AutoresServices(); } }
        #endregion

        /// <summary>
        /// Method to get a list of autor
        /// </summary>
        /// <returns>List of Autor</returns>
        public IEnumerable<Autor> GetAll()
        {
            return AutoresMap.Current.GetAll();
        }

        /// <summary>
        /// Method to get a autor
        /// </summary>
        /// <param name="id">Autor GID</param>
        /// <returns>An autor</returns>
        public Autor Find(Guid id)
        {
            return AutoresMap.Current.Find(x => x.GID == id).FirstOrDefault();
        }

        /// <summary>
        /// Method to get a autor
        /// </summary>
        /// <param name="id">Autor GID</param>
        /// <returns>An autor</returns>
        public List<Autor> FindByName(string autorName)
        {
            string[] names = autorName.Split(' ');

            List<List<Autor>> aLists = new List<List<Autor>>();
            List<Autor> Autors = new List<Autor>();

            foreach (string n in autorName.Split(' ').Where(nn => nn != string.Empty && nn.Length > 2))
            {
                aLists.Add(AutoresMap.Current.Find(p => p.Nombre.ToLower().Contains(n.ToLower())));
            }

            List<Autor> tempAutor = aLists.SelectMany(blp => blp).ToList();

            tempAutor.ForEach(p =>
            {
                if (Autors.Any(p2 => p2.GID.Equals(p.GID))) { return; }

                Autors.Add(p);
            });

            return Autors;
        }

        /// <summary>
        /// Method to record a new autor
        /// </summary>
        /// <param name="entity">Autor to be save</param>
        /// <returns>An autor saved</returns>
        public Autor Add(Autor entity)
        {
            return this.Add(new List<Autor>() { entity }).FirstOrDefault();
        }

        /// <summary>
        /// Method to adds a list of autor
        /// </summary>
        /// <param name="entities">List of autor to be save</param>
        /// <returns>List of autor saved</returns>
        public IEnumerable<Autor> Add(IEnumerable<Autor> entities)
        {
            List<Autor> result = new List<Autor>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    result = AutoresMap.Current.Add(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return result;
        }

        /// <summary>
        /// Method to update an autor
        /// </summary>
        /// <param name="entity">Autor to be updated</param>
        /// <returns>An autor updated</returns>
        public Autor Update(Autor entity)
        {
            return this.Update(new List<Autor>() { entity }).FirstOrDefault();
        }

        /// <summary>
        /// method to update a list of autor
        /// </summary>
        /// <param name="entities">List of autor</param>
        /// <returns>An list of autor</returns>
        public IEnumerable<Autor> Update(IEnumerable<Autor> entities)
        {
            List<Autor> result = new List<Autor>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    result = AutoresMap.Current.Update(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return result;
        }

        /// <summary>
        /// Method to remove an autor
        /// </summary>
        /// <param name="id">Guid: Autor Id</param>
        /// <returns>True if the autor was removed correctly</returns>
        public bool Delete(Guid id)
        {
            return this.Delete(new List<Guid>() { id });
        }

        /// <summary>
        /// Method to remove a list of autor
        /// </summary>
        /// <param name="ids">List of autor GIDs to be removed</param>
        /// <returns>True if autor was removed correctly</returns>
        public bool Delete(IEnumerable<Guid> ids)
        {
            bool result = false;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    //Get autors to be removed
                    var entities = AutoresMap.Current.Find(x=> ids.Contains(x.GID));

                    result = AutoresMap.Current.Delete(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return result;
        }
    }
}
