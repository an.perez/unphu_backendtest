﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using UBET.DATA;
using UBET.ENTITIES;
using UBET.SERVICES.Interfaces;

namespace UBET.SERVICES
{
    public class LibrosServices : IBasicMethods<Libro>
    {
        #region Static Properties
        public static LibrosServices Current { get { return new LibrosServices(); } }
        #endregion

        /// <summary>
        /// Method to get a list of books
        /// </summary>
        /// <returns>List of books</returns>
        public IEnumerable<Libro> GetAll()
        {
            return LibrosMap.Current.GetAll();
        }

        /// <summary>
        /// Method to get a book by GID
        /// </summary>
        /// <param name="id">Guid: Book GID</param>
        /// <returns>A book</returns>
        public Libro Find(Guid id)
        {
            return LibrosMap.Current.Find(x => x.GID == id).FirstOrDefault();
        }

        /// <summary>
        /// Method to get a book by name
        /// </summary>
        /// <param name="bookName">string: Book name</param>
        /// <returns>A list books</returns>
        public List<Libro> FindByName(string bookName)
        {
            string[] names = bookName.Split(' ');

            List<List<Libro>> bLists = new List<List<Libro>>();
            List<Libro> Books = new List<Libro>();

            foreach (string n in bookName.Split(' ').Where(nn => nn != string.Empty && nn.Length > 2))
            {
                bLists.Add(LibrosMap.Current.Find(p => p.Nombre.ToLower().Contains(n.ToLower())));
            }

            List<Libro> tempBooks = bLists.SelectMany(blp => blp).ToList();

            tempBooks.ForEach(p =>
            {
                if (Books.Any(p2 => p2.GID.Equals(p.GID))) { return; }

                Books.Add(p);
            });

            return Books;
        }

        /// <summary>
        /// Method to get a book by Epilogo
        /// </summary>
        /// <param name="id">string: Book epilogo</param>
        /// <returns>A list of book</returns>
        public List<Libro> FindByEpilogo(string epilogo)
        {
            string[] names = epilogo.Split(' ');

            List<List<Libro>> bLists = new List<List<Libro>>();
            List<Libro> Books = new List<Libro>();

            foreach (string n in epilogo.Split(' ').Where(nn => nn != string.Empty && nn.Length > 2))
            {
                bLists.Add(LibrosMap.Current.Find(p => p.Epilogo.ToLower().Contains(n.ToLower())));
            }

            List<Libro> tempBooks = bLists.SelectMany(blp => blp).ToList();

            tempBooks.ForEach(p =>
            {
                if (Books.Any(p2 => p2.GID.Equals(p.GID))) { return; }

                Books.Add(p);
            });

            return Books;
        }

        /// <summary>
        /// Method to add a new book
        /// </summary>
        /// <param name="entity">A book to be added</param>
        /// <returns>A book</returns>
        public Libro Add(Libro entity)
        {
            return this.Add(new List<Libro>() { entity }).FirstOrDefault();
        }

        /// <summary>
        /// Method to adds a list of book
        /// </summary>
        /// <param name="entities">List of book</param>
        /// <returns>A list f book</returns>
        public IEnumerable<Libro> Add(IEnumerable<Libro> entities)
        {
            List<Libro> result = new List<Libro>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    result = LibrosMap.Current.Add(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return result;
        }

        /// <summary>
        /// Method to update a book
        /// </summary>
        /// <param name="entity">Book to be updated</param>
        /// <returns>A book</returns>
        public Libro Update(Libro entity)
        {
            return this.Update(new List<Libro>() { entity }).FirstOrDefault();
        }

        /// <summary>
        /// Method to update a list of books
        /// </summary>
        /// <param name="entities">Books to be updated</param>
        /// <returns>A list of book</returns>
        public IEnumerable<Libro> Update(IEnumerable<Libro> entities)
        {
            List<Libro> result = new List<Libro>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    result = LibrosMap.Current.Update(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return result;
        }

        /// <summary>
        /// Method to remove a book
        /// </summary>
        /// <param name="id">Guid: Book GID</param>
        /// <returns>True if the book as removed</returns>
        public bool Delete(Guid id)
        {
            return this.Delete(new List<Guid>() { id });
        }

        /// <summary>
        /// Method to remove a list of book
        /// </summary>
        /// <param name="ids">List of book GID</param>
        /// <returns>True if the books was removed</returns>
        public bool Delete(IEnumerable<Guid> ids)
        {
            bool result = false;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    //Get books to removed
                    var entities = LibrosMap.Current.Find(x => ids.Contains(x.GID));

                    result = LibrosMap.Current.Delete(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return result;
        }
    }
}
