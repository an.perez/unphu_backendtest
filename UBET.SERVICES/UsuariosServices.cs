﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using UBET.DATA;
using UBET.ENTITIES;
using UBET.SERVICES.Interfaces;

namespace UBET.SERVICES
{
    public class UsuariosServices : IBasicMethods<Usuario>
    {
        #region Static Properties
        public static UsuariosServices Current { get { return new UsuariosServices(); } }
        #endregion

        public IEnumerable<Usuario> GetAll()
        {
            return UsuariosMap.Current.GetAll();
        }

        public Usuario Find(Guid id)
        {
            return UsuariosMap.Current.Find(x => x.GID == id).FirstOrDefault();
        }

        public Usuario Add(Usuario entity)
        {
            return this.Add(new List<Usuario>() { entity }).FirstOrDefault();
        }

        public IEnumerable<Usuario> Add(IEnumerable<Usuario> entities)
        {
            List<Usuario> result = new List<Usuario>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    result = UsuariosMap.Current.Add(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return result;
        }

        public Usuario Update(Usuario entity)
        {
            return this.Update(new List<Usuario>() { entity }).FirstOrDefault();
        }

        public IEnumerable<Usuario> Update(IEnumerable<Usuario> entities)
        {
            List<Usuario> result = new List<Usuario>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    result = UsuariosMap.Current.Update(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return result;
        }

        public bool Delete(Guid id)
        {
            return this.Delete(new List<Guid>() { id });
        }

        public bool Delete(IEnumerable<Guid> ids)
        {
            bool result = false;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var entities = UsuariosMap.Current.Find(x => ids.Contains(x.GID));
                    result = UsuariosMap.Current.Delete(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return result;
        }
    }
}
