﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UBET.SERVICES.Interfaces
{
    public interface IBasicMethods<T> where T : class
    {
        IEnumerable<T> GetAll();
        T Find(Guid id);
        T Add(T entity);
        IEnumerable<T> Add(IEnumerable<T> entities);
        T Update(T entity);
        IEnumerable<T> Update(IEnumerable<T> entities);
        bool Delete(Guid id);
        bool Delete(IEnumerable<Guid> ids);
    }
}
