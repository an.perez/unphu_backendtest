﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using UBET.DATA;
using UBET.ENTITIES;
using UBET.SERVICES.Interfaces;

namespace UBET.SERVICES
{
    public class TipoGeneralesServices : IBasicMethods<TipoGenerales>
    {
        #region Static Properties
        public static TipoGeneralesServices Current { get { return new TipoGeneralesServices(); } }
        #endregion

        /// <summary>
        /// Method to get a list og General Types
        /// </summary>
        /// <returns>List of General Types</returns>
        public IEnumerable<TipoGenerales> GetAll()
        {
            return TipoGeneralesMap.Current.GetAll();
        }

        /// <summary>
        /// Method to get a General Type by GID
        /// </summary>
        /// <param name="id">Guid: General Type GID</param>
        /// <returns>A General Type</returns>
        public TipoGenerales Find(Guid id)
        {
            return TipoGeneralesMap.Current.Find(x => x.GID == id).FirstOrDefault();
        }

        /// <summary>
        /// Method to get a General Type by parent GID
        /// </summary>
        /// <param name="parentId">Guid: Parent GID</param>
        /// <returns>A General Type</returns>
        public List<TipoGenerales> FindByParent(Guid parentId)
        {
            return TipoGeneralesMap.Current.Find(x => x.PadreId == parentId);
        }

        /// <summary>
        /// Method to get a General Type by GID
        /// </summary>
        /// <param name="id">Guid: General Type GID</param>
        /// <returns>A General Type</returns>
        public List<TipoGenerales> FindByParentCode(string parentCode)
        {
            var parentId = TipoGeneralesMap.Current.Find(x => x.Codigo.Equals(parentCode)).FirstOrDefault().GID;

            return TipoGeneralesMap.Current.Find(x => x.PadreId == parentId);
        }

        /// <summary>
        /// Method to add a new General Type
        /// </summary>
        /// <param name="entity">General Type</param>
        /// <returns>A General type</returns>
        public TipoGenerales Add(TipoGenerales entity)
        {
            return this.Add(new List<TipoGenerales>() { entity }).FirstOrDefault();
        }

        /// <summary>
        /// Method to adds a list of General Type
        /// </summary>
        /// <param name="entities">List of general types</param>
        /// <returns>A list of general type</returns>
        public IEnumerable<TipoGenerales> Add(IEnumerable<TipoGenerales> entities)
        {
            List<TipoGenerales> result = new List<TipoGenerales>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    result = TipoGeneralesMap.Current.Add(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return result;
        }

        /// <summary>
        /// Method to update a General Type
        /// </summary>
        /// <param name="entity">General Type to be updated</param>
        /// <returns>A General Type</returns>
        public TipoGenerales Update(TipoGenerales entity)
        {
            return this.Update(new List<TipoGenerales>() { entity }).FirstOrDefault();
        }

        /// <summary>
        /// Method to update a list to General Type
        /// </summary>
        /// <param name="entities">List of General Type</param>
        /// <returns>List a general type</returns>
        public IEnumerable<TipoGenerales> Update(IEnumerable<TipoGenerales> entities)
        {
            List<TipoGenerales> result = new List<TipoGenerales>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    result = TipoGeneralesMap.Current.Update(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return result;
        }

        /// <summary>
        /// Method to remove a General Type
        /// </summary>
        /// <param name="id">Guid: GeneralType GID</param>
        /// <returns>True if general type was removed</returns>
        public bool Delete(Guid id)
        {
            return this.Delete(new List<Guid>() { id });
        }

        /// <summary>
        /// Method to remove a list of general types
        /// </summary>
        /// <param name="ids">Guid: General Types GID</param>
        /// <returns>True if the general type was removed</returns>
        public bool Delete(IEnumerable<Guid> ids)
        {
            bool result = false;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var entities = TipoGeneralesMap.Current.Find(x => ids.Contains(x.GID));

                    result = TipoGeneralesMap.Current.Delete(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    throw ex;
                }
            }

            return result;
        }
    }
}
