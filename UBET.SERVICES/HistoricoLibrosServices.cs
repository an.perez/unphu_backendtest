﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UBET.DATA;
using UBET.ENTITIES.Views;

namespace UBET.SERVICES
{
    public class HistoricoLibrosServices
    {
        #region Static Properties
        public static HistoricoLibrosServices Current { get { return new HistoricoLibrosServices(); } }
        #endregion

        public List<HistoricoLibros> GetAll()
        {
            List<HistoricoLibros> result = new List<HistoricoLibros>();

            using (edmBiblioteca db = new edmBiblioteca())
            {
                result = db.HistoricoLibros.Select(c => c).ToList();
            }

            return result;
        }
    }
}
