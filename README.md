Pasos para ejecutar este proyecto

1) restaurar la base de datos en un servisor SQL Server
2) abrir la solucion con visual studio 2015 o superior
3) hacer click derecho en la solicion y seleccionar la opcion restaurar package
4) modificar la ruta de acceso a la base de datos en el key 'edmBiblioteca' que se encuentra en el archivo 'Web.Config' del proyecto API
5) presionar la tecla F5 para compilar he iniciar la ejecucion
6) una ves inicida la ejecucon añadir a la url '/swagger' para visualizar los controllers y metodos de la API
