﻿using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UBET.ENTITIES;
using UBET.ENTITIES.Views;
using UBET.SERVICES;

namespace UBET.API.Controllers
{
    public class BooksController : ApiController
    {
        [HttpGet]
        [Route("~/UNPHU/book/GetHistorico")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all Libros", typeof(List<HistoricoLibros>))]
        public IHttpActionResult GetHistorico()
        {
            IHttpActionResult result;

            result = Ok(HistoricoLibrosServices.Current.GetAll());


            return result;
        }

        [HttpGet]
        [Route("~/UNPHU/book/GetAll")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all Libros", typeof(List<Libro>))]
        public IHttpActionResult GetAll()
        {
            IHttpActionResult result;

            result = Ok(LibrosServices.Current.GetAll());


            return result;
        }

        [HttpGet]
        [Route("~/UNPHU/book/FindByName")]
        [SwaggerResponse(HttpStatusCode.OK, "List of Libro", typeof(List<Libro>))]
        public IHttpActionResult FindByName(string name)
        {
            IHttpActionResult result;

            result = Ok(LibrosServices.Current.FindByName(name));


            return result;
        }

        [HttpGet]
        [Route("~/UNPHU/book/FindByEpilogo")]
        [SwaggerResponse(HttpStatusCode.OK, "List of Libro", typeof(List<Libro>))]
        public IHttpActionResult FindByEpilogo(string epilogo)
        {
            IHttpActionResult result;

            result = Ok(LibrosServices.Current.FindByEpilogo(epilogo));


            return result;
        }

        [HttpGet]
        [Route("~/UNPHU/book/FindById/{id}")]
        [SwaggerResponse(HttpStatusCode.OK, "Get an Libros", typeof(Libro))]
        public IHttpActionResult FindById(Guid id)
        {
            IHttpActionResult result;

            result = Ok(LibrosServices.Current.Find(id));


            return result;
        }

        [HttpGet]
        [Route("~/UNPHU/book/Like/{id}")]
        [SwaggerResponse(HttpStatusCode.OK, "Get an Libros")]
        public void Like(Guid id)
        {
            var book = LibrosServices.Current.Find(id);

            book.ClasificacionTotal += 1;
            book.VotosAFavor += 1;

            LibrosServices.Current.Update(book);
        }

        [HttpGet]
        [Route("~/UNPHU/book/Dislike/{id}")]
        [SwaggerResponse(HttpStatusCode.OK, "Get an Libros")]
        public void Dislike(Guid id)
        {
            var book = LibrosServices.Current.Find(id);

            book.ClasificacionTotal += 1;
            book.VotosEnContra += 1;

            LibrosServices.Current.Update(book);

        }

        [HttpPost]
        [Route("~/UNPHU/book/Add")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all Libros", typeof(List<Libro>))]
        public IHttpActionResult Add([FromBody]Libro libro)
        {
            IHttpActionResult result;

            result = Ok(LibrosServices.Current.Add(libro));


            return result;
        }

        [HttpPut]
        [Route("~/UNPHU/book/Update")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all Libros", typeof(List<Libro>))]
        public IHttpActionResult Update([FromBody]Libro libro)
        {
            IHttpActionResult result;

            result = Ok(LibrosServices.Current.Update(libro));


            return result;
        }

        [HttpDelete]
        [Route("~/UNPHU/book/Delete")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all Libros", typeof(List<Libro>))]
        public IHttpActionResult Delete(Guid id)
        {
            IHttpActionResult result;

            result = Ok(LibrosServices.Current.Delete(id));


            return result;
        }
    }
}
