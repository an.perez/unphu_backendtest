﻿using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UBET.ENTITIES;
using UBET.SERVICES;

namespace UBET.API.Controllers
{
    public class UsuariosController : ApiController
    {
        [HttpGet]
        [Route("~/UNPHU/usuario/GetAll")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all Tipo Generale", typeof(List<Usuario>))]
        public IHttpActionResult GetAll()
        {
            IHttpActionResult result;

            result = Ok(UsuariosServices.Current.GetAll());


            return result;
        }

        [HttpGet]
        [Route("~/UNPHU/usuario/FindById/{id}")]
        [SwaggerResponse(HttpStatusCode.OK, "Get an Tipo Generale", typeof(Usuario))]
        public IHttpActionResult FindById(Guid id)
        {
            IHttpActionResult result;

            result = Ok(UsuariosServices.Current.Find(id));


            return result;
        }

        [HttpPost]
        [Route("~/UNPHU/usuario/Add")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all Tipo Generale", typeof(List<Usuario>))]
        public IHttpActionResult Add([FromBody]Usuario user)
        {
            IHttpActionResult result;

            result = Ok(UsuariosServices.Current.Add(user));


            return result;
        }

        [HttpPut]
        [Route("~/UNPHU/usuario/Update")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all Tipo Generale", typeof(List<Usuario>))]
        public IHttpActionResult Update([FromBody]Usuario user)
        {
            IHttpActionResult result;

            result = Ok(UsuariosServices.Current.Update(user));


            return result;
        }

        [HttpDelete]
        [Route("~/UNPHU/usuario/Delete")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all Tipo Generale", typeof(List<Usuario>))]
        public IHttpActionResult Delete(Guid id)
        {
            IHttpActionResult result;

            result = Ok(UsuariosServices.Current.Delete(id));


            return result;
        }
    }
}
