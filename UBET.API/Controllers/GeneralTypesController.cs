﻿using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UBET.ENTITIES;
using UBET.SERVICES;

namespace UBET.API.Controllers
{
    public class GeneralTypesController : ApiController
    {
        [HttpGet]
        [Route("~/UNPHU/general/GetAll")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all Tipo Generale", typeof(List<TipoGenerales>))]
        public IHttpActionResult GetAll()
        {
            IHttpActionResult result;

            result = Ok(TipoGeneralesServices.Current.GetAll());


            return result;
        }

        [HttpGet]
        [Route("~/UNPHU/general/FindByParentId")]
        [SwaggerResponse(HttpStatusCode.OK, "List of TipoGenerales", typeof(List<TipoGenerales>))]
        public IHttpActionResult FindByParentId(Guid parentId)
        {
            IHttpActionResult result;

            result = Ok(TipoGeneralesServices.Current.FindByParent(parentId));


            return result;
        }

        [HttpGet]
        [Route("~/UNPHU/general/FindByParentCode")]
        [SwaggerResponse(HttpStatusCode.OK, "List of TipoGenerales", typeof(List<TipoGenerales>))]
        public IHttpActionResult FindByParentCode(string parentCode)
        {
            IHttpActionResult result;

            result = Ok(TipoGeneralesServices.Current.FindByParentCode(parentCode));


            return result;
        }

        [HttpGet]
        [Route("~/UNPHU/general/FindById/{id}")]
        [SwaggerResponse(HttpStatusCode.OK, "Get an Tipo Generale", typeof(TipoGenerales))]
        public IHttpActionResult FindById(Guid id)
        {
            IHttpActionResult result;

            result = Ok(TipoGeneralesServices.Current.Find(id));


            return result;
        }

        [HttpPost]
        [Route("~/UNPHU/general/Add")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all Tipo Generale", typeof(List<TipoGenerales>))]
        public IHttpActionResult Add([FromBody]TipoGenerales generalType)
        {
            IHttpActionResult result;

            result = Ok(TipoGeneralesServices.Current.Add(generalType));


            return result;
        }

        [HttpPut]
        [Route("~/UNPHU/general/Update")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all Tipo Generale", typeof(List<TipoGenerales>))]
        public IHttpActionResult Update([FromBody]TipoGenerales generalType)
        {
            IHttpActionResult result;

            result = Ok(TipoGeneralesServices.Current.Update(generalType));


            return result;
        }

        [HttpDelete]
        [Route("~/UNPHU/general/Delete")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all Tipo Generale", typeof(List<TipoGenerales>))]
        public IHttpActionResult Delete(Guid id)
        {
            IHttpActionResult result;

            result = Ok(TipoGeneralesServices.Current.Delete(id));


            return result;
        }
    }
}
