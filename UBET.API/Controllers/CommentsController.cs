﻿using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UBET.ENTITIES;
using UBET.SERVICES;

namespace UBET.API.Controllers
{
    public class CommentsController : ApiController
    {
        [HttpGet]
        [Route("~/UNPHU/comment/GetAll")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all Comments", typeof(List<ComentariosLibrosAutores>))]
        public IHttpActionResult GetAll()
        {
            IHttpActionResult result;

            result = Ok(ComentariosLibrosAutoresServices.Current.GetAll());


            return result;
        }

        [HttpGet]
        [Route("~/UNPHU/comment/FindByBookId")]
        [SwaggerResponse(HttpStatusCode.OK, "List of Comments", typeof(List<ComentariosLibrosAutores>))]
        public IHttpActionResult FindByBookId(Guid bookId)
        {
            IHttpActionResult result;

            result = Ok(ComentariosLibrosAutoresServices.Current.FindByOwner(bookId));


            return result;
        }

        [HttpGet]
        [Route("~/UNPHU/comment/FindByAutorId")]
        [SwaggerResponse(HttpStatusCode.OK, "List of Comments", typeof(List<ComentariosLibrosAutores>))]
        public IHttpActionResult FindByAutorId(Guid autorId)
        {
            IHttpActionResult result;

            result = Ok(ComentariosLibrosAutoresServices.Current.FindByOwner(autorId));


            return result;
        }

        [HttpGet]
        [Route("~/UNPHU/comment/FindById/{id}")]
        [SwaggerResponse(HttpStatusCode.OK, "Get an Comments", typeof(ComentariosLibrosAutores))]
        public IHttpActionResult FindById(Guid id)
        {
            IHttpActionResult result;

            result = Ok(ComentariosLibrosAutoresServices.Current.Find(id));


            return result;
        }

        [HttpPost]
        [Route("~/UNPHU/comment/Add")]
        [SwaggerResponse(HttpStatusCode.OK, "Save a new Comment", typeof(List<ComentariosLibrosAutores>))]
        public IHttpActionResult Add([FromBody]ComentariosLibrosAutores comment)
        {
            IHttpActionResult result;

            result = Ok(ComentariosLibrosAutoresServices.Current.Add(comment));


            return result;
        }

        [HttpPut]
        [Route("~/UNPHU/comment/Update")]
        [SwaggerResponse(HttpStatusCode.OK, "Update a Comment", typeof(List<ComentariosLibrosAutores>))]
        public IHttpActionResult Update([FromBody]ComentariosLibrosAutores comment)
        {
            IHttpActionResult result;

            result = Ok(ComentariosLibrosAutoresServices.Current.Update(comment));


            return result;
        }

        [HttpDelete]
        [Route("~/UNPHU/comment/Delete")]
        [SwaggerResponse(HttpStatusCode.OK, "Remove a Comments", typeof(List<ComentariosLibrosAutores>))]
        public IHttpActionResult Delete(Guid id)
        {
            IHttpActionResult result;

            result = Ok(ComentariosLibrosAutoresServices.Current.Delete(id));


            return result;
        }
    }
}
