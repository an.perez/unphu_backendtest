﻿using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UBET.ENTITIES;
using UBET.SERVICES;

namespace UBET.API.Controllers
{
    public class AutorsController : ApiController
    {

        [HttpGet]
        [Route("~/UNPHU/autor/GetAll")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all autors", typeof(List<Autor>))]
        public IHttpActionResult GetAll()
        {
            IHttpActionResult result;

            result = Ok(AutoresServices.Current.GetAll());


            return result;
        }

        [HttpGet]
        [Route("~/UNPHU/autor/FindByName")]
        [SwaggerResponse(HttpStatusCode.OK, "List of autor", typeof(List<Autor>))]
        public IHttpActionResult FindByName(string name)
        {
            IHttpActionResult result;

            result = Ok(AutoresServices.Current.FindByName(name));


            return result;
        }

        [HttpGet]
        [Route("~/UNPHU/autor/FindById/{id}")]
        [SwaggerResponse(HttpStatusCode.OK, "Get an autors", typeof(Autor))]
        public IHttpActionResult FindById(Guid id)
        {
            IHttpActionResult result;

            result = Ok(AutoresServices.Current.Find(id));


            return result;
        }

        [HttpPost]
        [Route("~/UNPHU/autor/Add")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all autors", typeof(List<Autor>))]
        public IHttpActionResult Add([FromBody]Autor autor)
        {
            IHttpActionResult result;

            result = Ok(AutoresServices.Current.Add(autor));


            return result;
        }

        [HttpPut]
        [Route("~/UNPHU/autor/Update")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all autors", typeof(List<Autor>))]
        public IHttpActionResult Update([FromBody]Autor autor)
        {
            IHttpActionResult result;

            result = Ok(AutoresServices.Current.Update(autor));


            return result;
        }

        [HttpDelete]
        [Route("~/UNPHU/autor/Delete")]
        [SwaggerResponse(HttpStatusCode.OK, "Get all autors", typeof(List<Autor>))]
        public IHttpActionResult Delete(Guid id)
        {
            IHttpActionResult result;

            result = Ok(AutoresServices.Current.Delete(id));


            return result;
        }
    }
}
